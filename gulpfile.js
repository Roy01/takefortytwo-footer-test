const gulp = require('gulp'),
  sass = require('gulp-sass')(require('sass')),
  postcss = require('gulp-postcss'),
  cssnano = require('cssnano'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require('browser-sync').create();

const scssArray =  [
  'src/scss/common.scss',
];
const watchArray =  [
  'src/scss/common/_utils/css-reset.scss',
  'src/scss/common/variables.scss',
  'src/scss/common/footer.scss',
  'src/scss/common.scss',
  'dist/index.html'
];

const paths = {
  styles: {
    src: scssArray,
    dest: 'dist/css',
    watchArray: watchArray
  },
};


function style() {
  return gulp
    .src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(postcss([cssnano({
      preset: ['default', {
        discardComments: {
          removeAll: true,
        },
      }],
    })]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

function reload(done) {
  browserSync.reload();
  done();
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./dist",
      open: false
    },
  });
  gulp.watch(paths.styles.watchArray, style).on('change', browserSync.reload);
}

exports.watch = watch;
exports.style = style;

const build = gulp.parallel(style, watch, reload);

gulp.task('default', build);

